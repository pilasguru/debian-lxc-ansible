#!/bin/bash
# destroy-container.sh name
#

IPCONT=$(lxc-info -n ${1} -i | awk '{print $2}')
OUTPORT=$(iptables -L -n -v -t nat | grep ${IPCONT} | cut -d: -f 2 | awk '{print $1}')

echo "Delete iptables DNAT rule"
iptables  -t nat -D PREROUTING -p tcp -d 45.55.47.111 --dport ${OUTPORT} -j DNAT --to-destination ${IPCONT}:80

lxc-stop -n ${1}
lxc-destroy -n ${1} 
exit 0
