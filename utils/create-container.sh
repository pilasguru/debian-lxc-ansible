#!/bin/bash
# create-container.sh name
#

# Provider
lxc-create -n ${1} -t debian
lxc-start -n ${1}
echo "Waiting network start ..."
sleep 3
echo 'echo "root:root" | chpasswd' | lxc-attach -n ${1} -- /bin/bash

# Provision
lxc-attach -n ${1} apt-get update
lxc-attach -n ${1} -- apt-get install -y apache2

IPCONT=$(lxc-info -n ${1} -i | awk '{print $2}')
OUTPORT=$(shuf -i 2000-9999 -n 1)
iptables  -t nat -A PREROUTING -p tcp -d 45.55.47.111 --dport ${OUTPORT} -j DNAT --to-destination ${IPCONT}:80

echo "<html><H1>${1}</H1><p></p><pre>" > /var/lib/lxc/${1}/rootfs/var/www/html/index.html
lxc-info -n ${1} >> /var/lib/lxc/${1}/rootfs/var/www/html/index.html
echo "</pre></html>" >> /var/lib/lxc/${1}/rootfs/var/www/html/index.html

echo
echo "Connect to http://45.55.47.111:${OUTPORT} to access container ${IPCONT}:80"
echo $(date +"%x %X")" http://45.55.47.111:${OUTPORT} container ${1} ${IPCONT}" >> create-container.log

exit 0

