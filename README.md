
A Vagrantfile to configure a vanilla Debian 8 "Jessie" Linux box as a LXC capable server

Also download, install and run a first container of Debian Jessie.

It uses vagrant *ansible_local* provisioning.


## Installation

```
git clone https://gitlab.com/pilasguru/debian-lxc-ansible.git
cd debian-lxc-ansible
vagrant up
```
The installation is **idempotent**, then multiple `vagrant provision` may be run after installation.

## Use

```
$ vagrant ssh

vagrant@server:~$ sudo su -
root@server:~# lxc-ls
NAME      STATE    IPV4  IPV6  AUTOSTART
----------------------------------------
firstlxc  RUNNING  10.0.0.112  -     NO
root@server:~# lxc-console -n firstlxc

firstlxc login: root
Password: root
root@firstlxc:~#
```

## Costumization

It do not require additional configuration, but if you need to customize configuration, the installation 
uses two ansible roles:

* **lxcserver** to configure base LXC server
* **first-container** to install the first container

Inside each role there is a *default/main.yml* file with a first and simple aproach to customize installation.



## License

```
The MIT License (MIT)
 
Copyright (c) 2016 Rodolfo Pilas <rodolfo@pilas.guru>
```




